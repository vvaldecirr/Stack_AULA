package control;

import java.util.Scanner;

import model.StackVal;

public class Questao7 {

	public static void main(String[] args) {
		int x = 0;
		
		StackVal pilha = new StackVal(10);
		
		Scanner input	= new Scanner(System.in);
		
		int num = input.nextInt();
		
		while (num > 0) {
			num = input.nextInt();
			
			if(num != 3)
				pilha.pushElement(num);
			else {
				pilha.popTopElement();
				
				x = pilha.getTopValue();
			}
			
			System.out.println(x);
		}
	}

}
