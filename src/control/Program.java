package control;

import java.util.Scanner;

import model.StackVal;

/**
 * Program developed by student Valdecir - 5305737 (UNIGRANRIO) 
 * using the StackVal class made for Data Structure exercise class   
 * @author vvaldecirr
 * Mail: vvaldecirr@hotmail.com (Rio de Janeiro - March 2017)
 */
public class Program {

	public static void main(String[] args) throws Throwable {
		Scanner input	= new Scanner(System.in);
		
    	System.out.println("\n#################################");
    	System.out.println("#                               #");
    	System.out.println("#   StackVal (LLS tipo PILHA)   #");
    	System.out.println("#                               #");
    	System.out.println("#################################");

    	System.out.println("\nDigite o nº INTEIRO referente à capacidade da PILHA: ");
		int stackSize	= input.nextInt();
		
		StackVal stack	= new StackVal(stackSize);
		
		int menuOption;
		int result;
		char element;
		
		do{
			System.out.println("\n#################################");
			System.out.println("#    *-- MENU DE OPÇÕES --*     #");
			System.out.println("#################################");
			System.out.println("#   1 - Inserir novo caracter   #");
			System.out.println("#   2 - Buscar caracter         #");
			System.out.println("#   3 - Excluir caracter        #");
			System.out.println("#   4 - Mostrar PILHA           #");
			System.out.println("#   5 - Sair                    #");
			System.out.println("#################################");
			
			System.out.println("\nDigite o número da opção desejada: ");
			menuOption = input.nextInt();
			
			switch (menuOption) {
	        case 1:
	        	System.out.println("\n#################################");
	        	System.out.println("#   *-- OPÇÃO DE INSERÇÃO --*   #");
	        	System.out.println("#################################");
	            
				System.out.println("Digite o caracter a ser armazenado: ");
				element = input.next().charAt(0);
				
				if (!stack.pushElement(element)) 
					System.out.println("Pilha saturada! Impossível nova inserção.");
				else
					System.out.println("Inserção realizada com SUCESSO");
	            
				break;
	            
	        case 2:
	        	System.out.println("\n#################################");
	        	System.out.println("#    *-- OPÇÃO DE BUSCA --*     #");
	        	System.out.println("#################################");

				System.out.println("Digite o caracter a ser encontrado: ");
				element = input.next().charAt(0);
				
				result = stack.findElementByContent(element);
				
				if (result == -1) 
					System.out.println("Pilha VAZIA, impossível pesquizar.");
				else if (result == -2)
					System.out.println("Caractere NÃO encontrado");
				else
					System.out.println("O caractere encontra-se no índice de nº: "+result);
				
				break;
	        
	        case 3:
	        	System.out.println("\n#################################");
	        	System.out.println("#   *-- OPÇÃO DE EXCLUSÃO --*   #");
	        	System.out.println("#################################");

				System.out.println("Digite o caracter a ser excluído: ");
				element = input.next().charAt(0);
				
				result = stack.findElementByContent(element);
				
				if (result < 0) 
					System.out.println("Caractere inexistente, impossível excluir.");
				else {					
					if (stack.popElementByIndex(result)) 
						System.out.println("Caractere excluído com SUCESSO");
					else
						System.out.println("Erro na exclusão do caractere!");
				}
				
	            break;
	        
	        case 4:
	        	System.out.println("\n#################################");
	        	System.out.println("#   *-- OPÇÃO DE EXIBIÇÃO --*   #");
	        	System.out.println("#################################");
	        	
	        	String[] structure = stack.getStackStructure();
	        	
	        	int top		= stack.getTopIndex();
	        	int length	= structure.length;
	        	
	        	for (int i = length-1; i > -1; i--) {
	        		if (i == top)
	        			System.out.println("#      "+structure[i]+" <-- topo        #");
	        		else
	        			System.out.println("#      "+structure[i]+"                 #");
				}
	        	
	        	System.out.println("#################################");
	        	
	            break;

	        case 5:
	        	System.out.println("\n\n\n\n#################################");
	        	System.out.println("#                               #");
	        	System.out.println("#   *-- PROGRAMA ENCERRADO --*  #");
	        	System.out.println("#                               #");
	        	System.out.println("#################################");
	        	break;
	        default:
	            System.out.println("ENTRADA INVÁLIDA! - Tente novamente:");
			}	
			
		}while(menuOption < 5);
	}
}
