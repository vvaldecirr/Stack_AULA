package model;

/**
 * Class developed by student Valdecir - 5305737 (UNIGRANRIO) 
 * solving the STACK list of Data Structure exercise class   
 * @author vvaldecirr
 * Mail: vvaldecirr@hotmail.com (Rio de Janeiro - March 2017)
 */
public class StackVal {
	
	private char[] stack;
	private int top;

	public StackVal(int size) {
		this.stack	= new char[size];
		this.top	= -1;
	}

	public int getTopIndex() {
		return top;
	}
	
	public char getTopValue() {
		return this.stack[this.top];
	}
	
	//answer the status of stack
	public int getStackStatus() {
		if (this.top == this.stack.length-1)
			return 1; //stack full
		else
			if (this.top == -1)
				return -1; //stack empty
			else
				return 0; //stack on use
	}
	
//	//DEPRECATED:
//	//answer if the stack is full
//	public boolean isFull() {
//		if (this.top == this.stack.length-1)
//			return true;
//		else
//			return false;
//	}
//	
//	//answer if the stack is empty
//	public boolean isEmpty() {
//		if (this.stack.length == -1)
//			return true;
//		else
//			return false;
//	}	

	//insert elements into stack if not full
	public boolean pushElement(char element) {
		if (this.getStackStatus() == 1)
			return false;
		else {
			this.stack[++this.top] = element;
			return true;
		}
	}
	
	//remove elements from top of stack
	public boolean popTopElement() {
		if (this.getStackStatus() == -1)
			return false;
		else {
			this.top--;
			return true;
		}
	}
	
	//remove an element from stack keeping the integrity (no matter the position)
	public boolean popElementByIndex(int index) throws Throwable {
		if (this.getStackStatus() == -1) //if empty stack
			return false;
		else {
			if (this.getTopIndex() == 0 || this.getTopIndex() == index) //if the element is the first or last one from stack
				popTopElement();
			else {
				//1º instance an aux stack with the same length of main stack (recursive programming become more beautiful)
				StackVal auxStack = new StackVal(this.stack.length);
				
				//2º push each top value from main stack to aux one till reach the index
				for (int i = this.getTopIndex(); i > index; i--) {
					auxStack.pushElement(this.getTopValue());
					this.popTopElement();
				}
				
				//3º pop actual index from main stack
				popTopElement();
				
				//4º return all elements from aux stack pushing back to the main one
				for (int i = auxStack.getTopIndex(); i > -1; i--) {
					this.pushElement(auxStack.getTopValue());
					auxStack.popTopElement();
				}
				
				//5º destroy aux stack
				auxStack.finalize();
			}

			return true;
		}
	}
	
	//returns the index of element, if found, keeping the integrity
	public int findElementByContent(char element) throws Throwable {
		int index = -2; //value not found
		
		if (this.getStackStatus() == -1) 
			index = -1; //empty stack
		else {
			//1º instance an aux stack with the same length of main stack (recursive programming become more beautiful)
			StackVal auxStack = new StackVal(this.stack.length);
			
			//2º push each top value from main stack to aux one till find the character
			for (int i = this.getTopIndex(); i > -1; i--) {
				if (this.getTopValue() == element) {
					index = this.getTopIndex(); //element found
					i = -1;
				} else {
					auxStack.pushElement(this.getTopValue());
					this.popTopElement();
				}
			}
			
			//3º return all elements from aux stack pushing back to the main one
			for (int i = auxStack.getTopIndex(); i > -1; i--) {
				this.pushElement(auxStack.getTopValue());
				auxStack.popTopElement();
			}
			
			//4º destroy aux stack
			auxStack.finalize();
			
			return index;
		}
		
		return index;
	}
	
	//returns an already readable array of strings from stack's values with they respective indexes
	public String[] getStackStructure() throws Throwable {
		//1º instance an array of strings with the same length of main stack
		String[] structure = new String[this.stack.length];
		
		//2º instance an aux stack with the same length of main stack (recursive programming become more beautiful)
		StackVal auxStack = new StackVal(this.stack.length);
		
		//3º push each top value from main stack to array creating an already readable string format
		for (int i = this.stack.length-1; i > -1; i--) {
			String dots;
			
			//keeping layout lined
			if (i > 99)
				dots = "..";
			else if (i > 9)
				dots = "...";
			else
				dots = "....";
			
			//mounting the string
			if (this.getTopIndex() >= i) {
				structure[i] = i+dots+"["+this.getTopValue()+"]";
				auxStack.pushElement(this.getTopValue());
				this.popTopElement();
			} else 
				structure[i] = i+dots+"[_]";
		}	
		
		//4º return all elements from aux stack pushing back to the main one
		for (int i = auxStack.getTopIndex(); i > -1; i--) {
			this.pushElement(auxStack.getTopValue());
			auxStack.popTopElement();
		}
		
		//5º destroy aux stack
		auxStack.finalize();
		
		return structure;
	}
}
